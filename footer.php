<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="container">
    
        <div class="row">
        
            <div class="col-md-4 col-md-4 footerculumna">
                <img src="<?php bloginfo('template_directory'); ?>/images/diucon_logo.png">
                <br><br>
                <p>Empresa de Viajes y Turismo</p>
               <p> Leg: 13644 / Disp. 1048/08</p>
               <p> 9 de Julio 1086 - Esquel, Patagonia Argentina</p>
            </div>
            
            <div class="col-md-4 footerculumna">
                <h4>CONTACTO</h4><br>
                 <p>   <i class="fa fa-map-marker fa-fw"></i>&nbsp; 9 de Julio 1086 </p>
         <p>   <i class="fa fa-whatsapp fa-fw"></i>&nbsp; +54 9 2945 15050882</p>
        <p>    <i class="fa fa-phone fa-fw"></i>&nbsp; +54 (02945) 450108 </p>
          <p>  <i class="fa fa-envelope fa-fw"></i>&nbsp; info@diucon.com</p>       
            
            </div>
            
            <div class="col-md-4 footerculumna">
            
             
                 <h4>WEBS AMIGAS</h4><br>
				<p>Esquel Ski Club</p>
         <p>   Pantano Mágico</p>
        <p>    Hotel Sol Del Sur
          <p> Atalaya</p>
                <p> Agencia URK</p>
            
                
            </div>
        
        
        </div>
        
    </div>
    
</footer><!-- #colophon .site-footer -->
</div><!-- #page .hfeed .site -->

<?php wp_footer(); ?>

</body>
</html>