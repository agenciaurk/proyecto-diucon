<?php
/*
Template Name: Invierno template
*/
?>

<?php get_header(); ?>

<!-- BANNER -->
<div id="banner-pagina">
<div class="banner banner-single" style="background-image: url('<?php header_image(); ?>'); "> 
    <div class="banner-overlay">
    </div>
    <div class="titulo-banner">
        <h1><?php the_title(); ?>

        <br>

        </h1>
    </div>

</div> 
</div>
<!-- banner -->

<div class="container contenido-paginas">
	
	<div class="texto text-center">
	<h1>Conocé nuestras promociones</h1>
	<h3>hacé click en las imagenes para agrandarlas</h3>
</div>

<div class="row promociones">
	<div class="col-md-4">
		<a href="<?php bloginfo('template_directory'); ?>/images/dv_2015_lahoya.jpg" target="_blank">	
			<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/dv_2015_lahoya.jpg">
		</a>	
	</div>
	<div class="col-md-4">
		<a href="<?php bloginfo('template_directory'); ?>/images/dv_ski-weeks.jpg" target="_blank">		

				<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/dv_ski-weeks.jpg">

		</a>	
	</div>
	<div class="col-md-4">
		<a href="<?php bloginfo('template_directory'); ?>/images/dv_2015_rental.jpg" target="_blank">


				<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/dv_2015_rental.jpg">
		
		</a>
	</div>
</div>
<div class="row promociones">
	<div class="col-md-4">
		<a href="<?php bloginfo('template_directory'); ?>/images/dv_2015_sol-del-sur.jpg" target="_blank">

				<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/dv_2015_sol-del-sur.jpg">

		</a>	
	</div>
	<div class="col-md-4">
		<a href="<?php bloginfo('template_directory'); ?>/images/dv_2016_equipos-y-alojamiento.jpg" target="_blank">

				<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/dv_2016_equipos-y-alojamiento.jpg">

		</a>	
	</div>
	<div class="col-md-4">
		<a href="<?php bloginfo('template_directory'); ?>/images/dv_2015_montebianco.jpg" target="_blank">

				<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/dv_2015_montebianco.jpg">

		</a>	
	</div>
</div>


<div class="row">

<?php if (have_posts()) : while (have_posts()) : the_post();?>
	<?php the_content(); ?>
<?php endwhile; endif; ?>

</div>	
</div>



</div>
<?php get_footer(); ?>

