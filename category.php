<?php
/**
* A Simple Category Template
*/

get_header(); ?> 
<!-- BANNER -->

<div id="buscador">
<div class="banner">

<div class="titulo-banner">
    <h1>Búsqueda</h1>
</div>

<div class="container-fluid">
<div class="banner-overlay">
    <div class="row">
        <img src="<?php bloginfo('template_directory'); ?>/images/cabecera-busqueda.jpg">
    </div>
</div>
</div>
</div>
</div>
<!-- banner -->
<section id="primary" class="site-content categorias">
<div id="content" role="main" class="paquetes-destacados">

<?php 
// Check if there are any posts to display
if ( have_posts() ) : ?>

<header class="archive-header">
<h1 class="archive-title">Categoría:</h1>

<h3 style="color: #3389D7 ;">


<?php single_cat_title( '', false ); ?>
	
<?php $current_category = single_cat_title("", false); 

	echo $current_category;
?>

</h3>


<?php
// Display optional category description

if ( category_description() ) : ?>
	<div class="archive-meta"><?php echo category_description(); ?></div>	
<?php endif; ?>


</header>
<div class="row">
<?php

// The Loop
while ( have_posts() ) : the_post(); ?>
	
	<?php get_template_part( 'content-paquetes-destacados', get_post_format() ); ?>

<?php endwhile; 

else: ?>

	<?php get_template_part( 'no-results', 'search' ); ?>

<?php endif; ?>
</div>
</div>
</section>

<?php get_template_part( 'contacto' ); ?>
<?php get_template_part( 'footer' ); ?>
