<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 entrada">


    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"> 

    <article style="margin: 0px" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


		<div class="entry-content postImage">

		<div class="separador border-t"></div>

			<div class="entradaminiatura">

          <div class='texto-entrada'> 
            <h3><?php  the_title(); ?></h3>
          </div>
        
        <div class="imagenminiatura">   

          <?php 
          
          $compra = get_field("compraonline");
          if( $compra == "si" ) { 

          echo "

              <div class='package-ribbon-wrapper'>
                <div class='package-type normal-type'>Compra online</div>
                <div class='clear'></div>
                <div class='package-type-gimmick'></div><div class='clear'></div>
              </div>";

          } ?>




          <img class="img-responsive" src="<?php the_post_thumbnail('grid-post-image'); ?>
        </div>
			</div>

		</div>


<?php 

$selected = get_field('iconos');

if ($selected) {
  
  echo "<div class='iconos-paquete'>";
      
  foreach ($selected as $icono) {

      echo "<ul>";
        echo "<li><span class='icon-".$icono."'></span></li>";
      echo "</ul>";
  
  }

  echo "</div>";

}

?>















	</article>

	</a>

</div>
