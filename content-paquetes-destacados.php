<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 entrada">
  <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"> 
    <article style="margin: 0px" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="entry-content postImage">
    <div class="separador border-t"></div>
      <div class="entradaminiatura">




          <div class='texto-entrada'>
            <?php
			 $compra = get_field("precio");
            if( $compra ) {

              echo "<div class='texto-entrada precio'>
                  <div class='precio-destacado'>
                    <div class='precio-minimo''>DESDE: <span> $compra</span></div>
                  </div>
              </div>";
	  		}

          ?>

              <h3><?php  the_title(); ?></h3>
          </div>
      
        <div class="imagenminiatura">      
          <?php the_post_thumbnail('grid-post-image img-responsive'); ?>
        </div>
      </div>
    </div>
		
		<?php 

$selected = get_field('iconos');

if ($selected) {
  
  echo "<div class='iconos-paquete'>";
      
  foreach ($selected as $icono) {

      echo "<ul>";
        echo "<li><span class='icon-".$icono."'></span></li>";
      echo "</ul>";
  
  }

  echo "</div>";

}

?>


  </article>
  </a>
</div>


