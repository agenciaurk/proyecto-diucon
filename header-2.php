<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
  <title><?php
        
    global $page, $paged;
     
    wp_title( '|', true, 'right' );
     

    bloginfo( 'name' );
     

    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
    echo " | $site_description";
     

    if ( $paged >= 2 || $page >= 2 )
    echo ' | ' . sprintf( __( 'Page %s', 'shape' ), max( $paged, $page ) );
     
    ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <?php wp_head(); ?>


	</head>

  <body <?php body_class(isset($class) ? $class : ''); ?>>

    <div class="header-info">
    <div class="container">
    <div class="row">
      <i class="fa fa-clock-o" aria-hidden="true"></i>
      HORARIO DE ATENCIÓN: Lun. a sáb. 8:30 hs/ 16:30hs a 20:30hs
      
      <i class="fa fa-phone" aria-hidden="true"></i>
      +54 <span>(02945) 15547033</span>

      <i class="fa fa-envelope-o" aria-hidden="true"></i>
      info@diucon
    </div>  
    </div>
    </div>

    <nav class="navbar navbar-default" role="navigation" style="position: relative;">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo home_url(); ?>">
          <img src="<?php bloginfo('template_directory'); ?>/images/logo.png">
        </a>
      </div>
      <div class="collapse navbar-collapse" style="overflow-x: hidden;">
       <?php wp_nav_menu( array('menu' => 'Main', 'menu_class' => 'nav navbar-nav navbar-left', 'depth'=> 3, 'container'=> false, 'walker'=> new Bootstrap_Walker_Nav_Menu)); ?>
      </div><!-- /.navbar-collapse -->
   </nav>


<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script>
//$( ".navbar-toggle" ).click(function() {

  //$( '.navbar' ).css('background-color','black');
//});
</script>