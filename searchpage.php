<?php
/*
Template Name: Search Page
*/
?>

<?php get_header(); ?>

<!-- BANNER -->

<div class="banner">
<div class="container-fluid">
<div class="row">
	<img src="<?php bloginfo('url'); ?>/images/banner.jpg">
</div>
</div>
</div>

<!-- banner -->
<div class="paquetes-destacados">
<div class="container-fluid">
<div class="texto text-center">
	<h1>PAQUETES DESTACADOS</h1>
	<h4>BUSCÁ TU PRÓXIMO VIAJE CON DIUCÓN: </h4> <?php get_search_form(); ?>
</div>
<div class="row">
	
	<?php if ( have_posts() and query_posts('')) : while ( have_posts() ) : the_post(); ?>
    	<?php get_template_part( 'content-paquetes-destacados', get_post_format() ); ?>
    	<?php endwhile; ?>
    <?php endif; ?>

</div>	
</div>
</div>

</div>
<?php get_footer(); ?>
