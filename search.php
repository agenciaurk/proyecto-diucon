<?php

get_header(); ?>
 
<!-- BANNER -->
<div id="buscador">
<div class="banner">

<div class="titulo-banner">
    <h1>Búsqueda</h1>
</div>

<div class="container-fluid">
<div class="banner-overlay">
    <div class="row">
        <img src="<?php bloginfo('template_directory'); ?>/images/cabecera-busqueda.jpg">
    </div>
</div>
</div>
</div>
</div>
<!-- banner -->

        <section id="primary" class="content-area">
            <div id="content" class="site-content" role="main" style="margin-top: 20px;">
 
            <?php if ( have_posts() ) : ?>
 
                <header class="page-header">
                    <h3 class="page-title" style="text-align:center;"><?php printf( __( '<span>Resultados de tu busqueda:</span><br> %s' ), '<span style="padding-top:0px; padding-bottom:3px; color: #3389D7 ; text-transform:lowercase;">' . get_search_query() . '</span>' ); ?></h3>
                </header><!-- .page-header -->
 
                <?php //shape_content_nav( 'nav-above' ); ?>

<div class="paquetes-destacados">    
    <div class="container-fluid">
     <div class="row">
                <?php /* Start the Loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>

                    <?php get_template_part( 'content-paquetes', 'search' ); ?>

                <?php endwhile; ?>

                <?php //shape_content_nav( 'nav-below' ); ?>

            <?php else : ?>
                
                <?php get_template_part( 'no-results', 'search' ); ?>

            <?php endif; ?>
         </div>
    </div>

</div>


            </div><!-- #content .site-content -->
        </section><!-- #primary .content-area -->
 
</div>
<?php get_template_part( 'contacto' ); ?>
<?php get_template_part( 'footer' ); ?>