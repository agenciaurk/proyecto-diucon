
<div class="banner banner-single" style="background-image: url('<?php header_image(); ?>'); "> 
    <div class="banner-overlay">
    </div>
    <div class="titulo-banner">
        <h1><?php the_title(); ?>

        <br>




<?php 

$selected = get_field('iconos');

if ($selected) {
  
  echo "<div class='icon'>";
      
  foreach ($selected as $icono) {

        echo "<span class='icon icon-".$icono."'></span>";
  
  }

  echo "</div>";

}

?>


        </h1>
    </div>

</div> 
</div>
</div>

<div class="container iconos-single text-center">
    <div class="row ">
        <div class="col-sm-4 col-md-3 col">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            <br><p>FECHA DE SALIDA<p>

              <?php 
              $fecha = get_field("fecha-salida");
              if( $fecha ) {
				  
              echo "<p class='field-dato'>$fecha</p>";
            } ?>

        </div>
        <div class="col-sm-4 col-md-3 col">
            <i class="fa fa-clock-o" aria-hidden="true"></i>
            <br><p>DURACIÓN<p>

              <?php 
              $duracion = get_field("duracion");
              if( $duracion ) {
             
              echo "<p class='field-dato'>$duracion</p>";
              } ?>



        </div>
        <div class="col-sm-4 col-md-3 col col-3" style="border: none;">
            <i class="fa fa-money" aria-hidden="true"></i>
            <br><p>PRECIO<p>


              <?php 
              $precio = get_field("precio");
              if( $precio ) {
             
                echo "<p class='field-dato'>$precio</p>";
              } ?>



        </div>
        <div class="col-sm-12 col-md-3 consulta">
           <a href="#contacto"> CONSULTAR DISPONIBILIDAD </a>
        </div>
    </div>
</div>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
    <div class="container entradasingle">
        <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="entry-content">
                <div class="contenido">
                    <p><?php the_content(); ?></p>




                </div>
            </div>
        </div>
        </div>
		
		<div class="container-fluid fotosentrada">
		
		
			<div class="row">
			
			<div class="col-sm-4 col-md-4">	
				<?php 
              $imagen1 = get_field("imagen1");
              if( $imagen1 ) {
             
                echo "<img class='img-responsive imagengaleria' src='$imagen1'>";
              } ?>
				
		</div>
			<div class="col-sm-4 col-md-4">
				
					  <?php 
              $imagen1 = get_field("imagen2");
              if( $imagen1 ) {
             
                echo "<img class='img-responsive imagengaleria' src='$imagen1'>";
              } ?>
		
				
				</div>
			<div class="col-sm-4 col-md-4">
				
					  <?php 
              $imagen1 = get_field("imagen3");
              if( $imagen1 ) {
             
                echo "<img class='img-responsive imagengaleria' src='$imagen1'>";
              } ?>
		
				
				</div>
			
			
			</div>
			
		
		</div>
	
		
		<div class="container-fluid compraonlineentrada paquetesalaventa"> <div class="row"> <?php $pasaje1=get_field( "pasaje1" );$precio1=get_field( "precio1" );$enlace1=get_field( "enlace1" );$pasaje2=get_field( "pasaje2" );$precio2=get_field( "precio2" );$enlace2=get_field( "enlace2" );$pasaje3=get_field( "pasaje3" );$precio3=get_field( "precio3" );$enlace3=get_field( "enlace3" );$pasaje4=get_field( "pasaje4" );$precio4=get_field( "precio4" );$enlace4=get_field( "enlace4" );$pasaje5=get_field( "pasaje5" );$precio5=get_field( "precio5" );$enlace5=get_field( "enlace5" ); if( $pasaje1){echo '<div class="titulocomprapaquetes">¡COMPRÁ ONLINE!</div>','<div class="col-md-4 col-sm-6"><div class="pasaje">',$pasaje1,'</div></div>';}else{echo '';}if( $precio1){echo '<div class="col-md-2 col-sm-6"><div class="pasaje">',$precio1,'</div></div>';}else{echo '';}if( $enlace1){echo '<div class="col-md-6 col-sm-12"><div class="enlacepasaje">','<a href="',$enlace1,'">¡COMPRÁ ONLINE AHORA!</a>','</div></div>';}else{echo '';}if( $pasaje2){echo '<div class="col-md-4 col-sm-6"><div class="pasaje">',$pasaje2,'</div></div>';}else{echo '';}if( $precio2){echo '<div class="col-md-2 col-sm-6"><div class="pasaje">',$precio2,'</div></div>';}else{echo '';}if( $enlace2){echo '<div class="col-md-6 col-sm-12"><div class="enlacepasaje">','<a href="',$enlace2,'">¡COMPRÁ ONLINE AHORA!</a>','</div></div>';}else{echo '';}if( $pasaje3){echo '<div class="col-md-4 col-sm-6"><div class="pasaje">',$pasaje3,'</div></div>';}else{echo '';}if( $precio3){echo '<div class="col-md-2 col-sm-6"><div class="pasaje">',$precio3,'</div></div>';}else{echo '';}if( $enlace3){echo '<div class="col-md-6 col-sm-12"><div class="enlacepasaje">','<a href="',$enlace3,'">¡COMPRÁ ONLINE AHORA!</a>','</div></div>';}else{echo '';}if( $pasaje4){echo '<div class="col-md-4 col-sm-6"><div class="pasaje">',$pasaje4,'</div></div>';}else{echo '';}if( $precio4){echo '<div class="col-md-2 col-sm-6"><div class="pasaje">',$precio4,'</div></div>';}else{echo '';}if( $enlace4){echo '<div class="col-md-6 col-sm-12"><div class="enlacepasaje">','<a href="',$enlace4,'">¡COMPRÁ ONLINE AHORA!</a>','</div></div>';}else{echo '';}if( $pasaje5){echo '<div class="col-md-4 col-sm-6"><div class="pasaje">',$pasaje5,'</div></div>';}else{echo '';}if( $precio5){echo '<div class="col-md-2 col-sm-6"><div class="pasaje">',$precio5,'</div></div>';}else{echo '';}if( $enlace5){echo '<div class="col-md-6 col-sm-12"><div class="enlacepasaje">','<a href="',$enlace5,'">¡COMPRÁ ONLINE AHORA!</a>','</div></div>';}else{echo '';}?> </div></div>
		
		
    </div>

	
	

</article><!-- #post-<?php the_ID(); ?> -->
