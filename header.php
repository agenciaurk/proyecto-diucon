<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
  <title><?php
        
    global $page, $paged;
     
    wp_title( '|', true, 'right' );
     

    bloginfo( 'name' );
     

    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
    echo " | $site_description";
     

    if ( $paged >= 2 || $page >= 2 )
    echo ' | ' . sprintf( __( 'Page %s', 'shape' ), max( $paged, $page ) );
     
    ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
    <?php wp_head(); ?>


	</head>

  <body <?php body_class(isset($class) ? $class : ''); ?>>

    <div class="header-info">
    <div class="container-fluid">
    <div class="row">
      <i class="fa fa-clock-o" aria-hidden="true"></i>
      HORARIO DE ATENCIÓN: Lun. a sáb. 8:30 hs a 12:30 / 16:30hs a 20:30hs
      <i class="fa fa-phone" aria-hidden="true"></i>
      +54 <span>(02945) 15050882</span>

      <i class="fa fa-envelope-o" aria-hidden="true"></i>
      info@diucon
    </div>  
    </div>
    </div>

    <nav class="navbar navbar-default" role="navigation" style="z-index: 999999999999999">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo home_url(); ?>" style="padding: 25px 20px !important;">
          <img src="<?php bloginfo('template_directory'); ?>/images/logo.png">
        </a>
      </div>
      <div class="collapse navbar-collapse" style="overflow-x: hidden;">
       <?php wp_nav_menu( array('menu' => 'Main', 'menu_class' => 'nav navbar-nav navbar-left', 'depth'=> 3, 'container'=> false, 'walker'=> new Bootstrap_Walker_Nav_Menu)); ?>
      

        <div class="iconos" style="color: white">

            <ul class="fh5co-special" data-offcanvass="yes" style="float: right; width: auto; ">
              
              <a href="https://www.facebook.com/diuconviajes"  TARGET="_blank">
                  <i style="margin-right:10px; " class="fa fa-facebook-square fa-3x faheader"></i>
              </a>  
              
              <a href="https://www.youtube.com/channel/UC2Ceh4d81evwwewUvNQeqVQ"  TARGET="_blank">
                <i style="margin-right:15px;" class="fa fa-youtube-play fa-3x faheader"></i>
              </a>

              <i data-toggle="modal" data-target="#myModal" class="fa fa-share-alt-square fa-3x botoncompartir" ></i>
            </ul>
              
              
            
          </div>

</div><!-- /.navbar-collapse -->

</nav>


<?php

function dameURL(){
    $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    return $url;
}
$url = dameURL();



#$site_description = get_bloginfo( 'description', 'display' );

#bloginfo( 'name' );

$twtTitle = wp_title('', false );
if ($twtTitle == '') {
  $twtTitle = ' Inicio';
}

$url = 'Diucon'.$twtTitle.' '.$url;
?>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" style="z-index: 9999999999999999999;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">&#161;Compartí esta pagina con tus amigos y familiares&#33;</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
            <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo $url; ?>">
              <i class="fa fa-facebook-square fa-4x"></i>
            </a>

            <a style="text-decoration: none;" href="http://twitter.com/home?status=<?php echo $url; ?>" target="_blank">
              <i class="fa fa-twitter-square fa-4x"></i>
            </a>

            <a href="whatsapp://send?text=Diucon |<?php echo $twtTitle; ?> – <?php urlencode(the_permalink()); ?>" data-action="share/whatsapp/share">
              <i class="fa fa-whatsapp fa-4x"></i>
            </a>
          </div>
        </div>
      

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>







<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script>
//$( ".navbar-toggle" ).click(function() {

  //$( '.navbar' ).css('background-color','black');
//});
</script>