<a style="archorlink" name="contacto"></a> 
<div class="contacto">
	
	<div class="container" style="padding-bottom: 27px;">
		<div class="row">
			<div class="col-md-offset-4 col-md-8">
				<h2 class="titulo-contacto text-center">CONTACTATE CON NOSOTROS</h2>
			</div>
		</div>
		<div class="row">

			<div class="col-sm-4 col-md-4">
				<div class="container-fluid medioscontacto">
					
					<span class="fa-stack fa-lg"> <i class="fa fa-square fa-stack-2x"></i> <i style="color: #2a3457;" class="fa fa-map-marker fa-stack-1x fa-inverse"></i> </span>&nbsp; 9 de Julio  <br>
					<span class="fa-stack fa-lg"> <i class="fa fa-square fa-stack-2x"></i> <i style="color: #2a3457;" class="fa fa-whatsapp fa-stack-1x fa-inverse"></i> </span>&nbsp; +54 9 2945 15050882<br>
					<span class="fa-stack fa-lg"> <i class="fa fa-square fa-stack-2x"></i> <i style="color: #2a3457;" class="fa fa-phone fa-stack-1x fa-inverse"></i> </span>&nbsp; +54 (02945) 450108 <br>
					<span class="fa-stack fa-lg"> <i class="fa fa-square fa-stack-2x"></i> <i style="color: #2a3457;" class="fa fa-envelope fa-stack-1x fa-inverse"></i> </span>&nbsp; info@diucon.com<br>

				</div>
			</div>
			
				<form action="https://formspree.io/esquelskiclub@gmail.com" method="POST">

						<div class="col-sm-8 col-md-8">

							<div class="container-fluid">
							

								
								<div class="row">			

									<div class="col-sm-5 col-md-5 limpiarpadding2">

										<div class="container-fluid">

											<div class="row">

												<div class="col-md-12 limpiarpadding">
													
													<div class="widthtotal"><input id="nombreapellido" placeholder="Nombre y Apellido" name="nombreapellido" type="text">	</div>
													
												</div>
												
												<div class="col-md-12 limpiarpadding">
													
														<div class="widthtotal"><input id="email" placeholder="E-mail" name="_replyto" type="email"></div>
													
												</div>
												
												<div class="col-md-12 limpiarpadding">
													
														<div class="widthtotal"><input id="telefono" placeholder="Teléfono" name="telefono" type="text"> </div>
													
												</div>

											</div>

										</div>

									</div>

									<div class="col-sm-7 col-md-7 limpiarpadding">

										<div class="container-fluid">

											<div class="row">

												<div class="col-md-12 limpiarpadding">
													
													  <textarea id="mensaje" placeholder="Tu mensaje" name="mensaje"></textarea>
									
												</div>
												
												

											</div>

										</div>

									</div>

								</div>
							</div>
							
							<div class="col-md-12 botonsend limpiarpadding">
												
								<input data-toggle="modal" data-target="#procesando"  class="btn btn-xl" type="submit">
							
							</div>

						</div>
					
					<input name="_next" value="http://esquelskiclub.com/gracias" type="hidden">
					
					<div id="procesando" class="modal fade" role="dialog">
  					<div class="modal-dialog">

    				<!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align:center;">PROCESANDO TU COMPRA</h4>
      </div>
      <div class="modal-body" style="text-align:center;">
		  <i style="font-size: 90px;" class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
<span class="sr-only">Procesando...</span>
		  <br><br>
        <p>Estamos procesando tu compra.<br>Esto puede demorar unos segundos...</p>
		 
      </div>
      
    </div>

  </div>
</div>
				
		</div>

	</div>

</div>
	</form>


  
 