<?php get_header(); ?>

<!-- BANNER -->

<div class="banner">

<div class="titulo-banner">
	<h1>INVIERNO 2016</h1>
	<a href="<?php bloginfo('url'); ?>/invierno-2016">
		<button class="boton">VER MAS</button>
	</a>
</div>

<div class="container-fluid">
<div class="banner-overlay">
	<div class="row">
		<img src="<?php bloginfo('template_directory'); ?>/images/banner.jpg">
	</div>
</div>
</div>
</div>
<!-- banner -->

<!-- PAQUETES -->

<div class="paquetes">
<div class="container-fluid">
<div class="row">

	<?php 
    
    if ( have_posts() and query_posts('showposts=4&category_name=destacados')) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'content-paquetes', get_post_format() ); ?>
    
    <?php endwhile; ?>
    <?php endif; ?>


</div>
</div>
</div>
</div>

<!-- paquetes -->

<!-- MERCADO PAGO -->

<div class="mercado-pago" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/parallax-home.jpg');">
<div class="container-fluid">
<div class="row">
	<div class="col-xs-12 col-md-6">
		<script id="despegar-root-js" type="text/javascript" src="//www.despegar.com.ar/comunidadafiliados/widgetGenerator/widgetCore.js" data-id="30167" data-context-path="//www.despegar.com.ar/comunidadafiliados"></script>
	</div>
</div>
</div>
</div>
<!-- mercado pago -->

<!-- PAQUETES DESTACADOS -->

<div class="paquetes-destacados">
<div class="container-fluid">
<div class="texto text-center">
	<h1>PAQUETES DESTACADOS</h1>
	<h4>BUSCÁ TU PRÓXIMO VIAJE CON DIUCÓN: </h4> <?php get_search_form(); ?>
</div>
<div class="row">


<?php echo do_shortcode('[tabby title="BRASIL"]'); ?>

<div class="tab-style">
	<div class="row row-paquetes">
		
		<?php if ( have_posts() and query_posts('showposts=4&category_name=brasil')) : while ( have_posts() ) : the_post(); ?>
	    	<?php get_template_part( 'content-paquetes-destacados', get_post_format() ); ?>
	    	<?php endwhile; ?>

	    <?php endif; ?>
		
	</div>
	<div class="row row-vermas">
		<a href="<?php bloginfo('url'); ?>/category/disney">VER MÁS</a>
	</div>
</div>
	
<?php echo do_shortcode('[tabby title="ARGENTINA"]'); ?>
	
	<?php if ( have_posts() and query_posts('showposts=4&category_name=destacados')) : while ( have_posts() ) : the_post(); ?>
    	<?php get_template_part( 'content-paquetes-destacados', get_post_format() ); ?>
    	<?php endwhile; ?>
    <?php endif; ?>

	
<?php echo do_shortcode('[tabby title="GRUPALES"]'); ?>
	<?php if ( have_posts() and query_posts('showposts=4&category_name=destacados')) : while ( have_posts() ) : the_post(); ?>
    	<?php get_template_part( 'content-paquetes-destacados', get_post_format() ); ?>
    	<?php endwhile; ?>
    <?php endif; ?>
	
<?php echo do_shortcode('[tabby title="EUROPA"]'); ?>
	<?php if ( have_posts() and query_posts('showposts=4&category_name=brasil')) : while ( have_posts() ) : the_post(); ?>
    	<?php get_template_part( 'content-paquetes-destacados', get_post_format() ); ?>
    	<?php endwhile; ?>
    <?php endif; ?>
	
<?php echo do_shortcode('[tabby title="CARIBE"]'); ?>
	
	<?php if ( have_posts() and query_posts('showposts=4&category_name=destacados')) : while ( have_posts() ) : the_post(); ?>
    	<?php get_template_part( 'content-paquetes-destacados', get_post_format() ); ?>
    	<?php endwhile; ?>
    <?php endif; ?>
	
<?php echo do_shortcode('[tabby title="ESQUEL"]'); ?>
	<?php if ( have_posts() and query_posts('showposts=4&category_name=brasil')) : while ( have_posts() ) : the_post(); ?>
      <?php get_template_part( 'content-paquetes-destacados', get_post_format() ); ?>
  
    <?php endwhile; ?>
    <?php endif; ?>
	
<?php echo do_shortcode('[tabby title="EUROPA"]'); ?>
	
	<?php if ( have_posts() and query_posts('showposts=4&category_name=peru')) : while ( have_posts() ) : the_post(); ?>
    	<?php get_template_part( 'content-paquetes-destacados', get_post_format() ); ?>
    	<?php endwhile; ?>
    <?php endif; ?>

<?php echo do_shortcode('[tabbyending]'); ?>

</div>	
</div>
</div>

<?php get_template_part( 'contacto' ); ?>
<?php get_template_part( 'footer' ); ?>
